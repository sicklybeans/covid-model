import numpy as np
from typing import ClassVar

class ConnectivityDistribution(object):
  mean: float

  def __init__(self, mean: float) -> None:
    self.mean = mean

  def generate(self, pop_size: int) -> np.ndarray:
    pass

  def __str__(self) -> str:
    return '%s' % self.__class__.__name__

class Flat(ConnectivityDistribution):

  def generate(self, pop_size: int) -> np.ndarray:
    return np.array([int(self.mean) for _ in range(pop_size)])

class Poisson(ConnectivityDistribution):

  def generate(self, pop_size: int) -> np.ndarray:
    return np.random.poisson(lam=self.mean, size=pop_size)

class Geometric(ConnectivityDistribution):

  def generate(self, pop_size: int) -> np.ndarray:
    return np.random.geometric(1/self.mean, size=pop_size)

class Gamma(ConnectivityDistribution):
  shape: float

  def __init__(self, mean: float, shape: float) -> None:
    super().__init__(mean)
    self.shape = shape

  def generate(self, pop_size: int) -> np.ndarray:
    theta = self.mean / self.shape
    return np.round_(np.random.gamma(self.shape, scale=theta, size=pop_size)).astype(int)

  def __str__(self) -> str:
    return '%s (k=%.1f)' % (self.__class__.__name__, self.shape)

