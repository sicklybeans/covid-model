import seaborn as sns
import matplotlib.pyplot as plt
from sim import *

def make_connectivity_hist(out: Output, ax, clr=None) -> None:
  ax.set_xlabel(r'number of daily connections')
  ax.set_ylabel(r'frequency')
  ax.set_title(r'Connectivity distribution')
  if out.p.connectivity_distribution.__class__.__name__ == 'Gamma':
    sns.distplot(out.p.connectivity, 40, ax=ax)
  else:
    sns.distplot(out.p.connectivity, ax=ax)
  # ax.plot(out.times, 1.0 - out.frac_uninfected, color=clr, label='Infected (cumulative)')
  # return 1.0 - out.frac_uninfected[-1]

def make_penetration_plot(out: Output, ax, clr=None) -> None:
  ax.set_xlabel(r'time (days)')
  ax.set_ylabel(r'population fraction')
  if not clr:
    ax.set_title(r'Cumulative infections')
  ax.set_ylim(0.0, 1.0)
  sns.lineplot(out.times, 1.0 - out.frac_uninfected, color=clr, label='Cumulative infections', ax=ax)

def make_infection_plot(out: Output, ax, clr=None) -> None:
  ax.set_xlabel(r'time (days)')
  ax.set_ylabel(r'population fraction')
  if not clr:
    ax.set_title(r'Active infections')
  ax.set_ylim(0.0, 1.0)
  sns.lineplot(out.times, out.frac_infected, color=clr, label='Infections', ax=ax)

def make_immunity_plot(out: Output, ax, clr=None) -> None:
  ax.set_xlabel(r'time (days)')
  ax.set_ylabel(r'population fraction')
  if not clr:
    ax.set_title(r'Immunity')
  ax.set_ylim(0.0, 1.0)
  sns.lineplot(out.times, out.frac_immune, color=clr, label='Immunity', ax=ax)

def make_rate_plot(out: Output, ax) -> None:
  ax.set_xlabel(r'time (days)')
  ax.set_ylabel(r'population fraction')
  ax.set_title(r'Rate of infection/recovery')

  inf_increase_rate = out.new_infections / out.p.size
  inf_recovery_rate = out.new_recoveries / out.p.size
  diff_rate = inf_increase_rate - inf_recovery_rate
  sns.lineplot(out.times, inf_increase_rate, color='red', label=r'new infections', ax=ax)
  sns.lineplot(out.times, inf_recovery_rate, color='blue', label=r'new recoveries', ax=ax)
  sns.lineplot(out.times, diff_rate, color='black', label=r'net change', ax=ax)
  ax.legend()

def make_stats_box(out: Output, ax) -> None:
  ax.set_xticks([])
  ax.set_yticks([])
  ax.set_title('Statistics')
  textstr = '\n'.join([
    r'$r_0=%.2f$' % out.p.mean_r_value,
    r'immunity threshold $=%.1f \%%$' % (100 * (1.0 - out.frac_uninfected[-2])),
    r'peak infectivity $=%.1f \%%$' % (100 * max(out.frac_infected))
  ])
  props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
  ax.text(0.025, 0.95, textstr, transform=ax.transAxes, fontsize=14, verticalalignment='top', bbox=props)

def make_plots(out: Output) -> None:
  print('Analyzing for parameter space:')
  print(out.p)

  fig, axs = plt.subplots(2, 2, figsize=(8, 8))

  make_penetration_plot(out, axs[0,0], clr='blue')
  make_infection_plot(out, axs[0,0], clr='red')
  make_immunity_plot(out, axs[0,0], clr='green')
  make_rate_plot(out, axs[1,0])
  make_connectivity_hist(out, axs[0,1])
  make_stats_box(out, axs[1,1])
  label = '%s $N=%d$, $M=%.1f$, $T=%.1f$' % (out.p.connectivity_distribution, out.p.size, out.p.mean_n_value, out.p.infection_time)
  fig.suptitle(label)
  axs[0,0].legend()
  plt.tight_layout()
  plt.show()

if __name__ == '__main__':
  make_plots(Output.load(sys.argv[1]))

