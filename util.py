def prompt_yes_no(question: str, default: bool=None) -> bool:
  """Asks a yes/no question and returns either True or False."""
  prompt = (default is True and 'Y/n') or (default is False and 'y/N') or 'y/n'
  valid = {'yes': True, 'ye': True, 'y': True, 'no': False, 'n': False}

  while True:
    choice = input(question + ' ' + prompt + ': ').lower()

    if not choice and default is not None:
      return default
    if choice in valid:
      return valid[choice]
    else:
      print('Invalid reponse')

def human_size(num_bytes: int) -> str:
  if num_bytes < 1000 * 1:
    return '%d B' % num_bytes
  elif num_bytes < 1000 ** 2:
    return '%.1f kB' % (num_bytes / (1000 ** 1))
  elif num_bytes < 1000 ** 3:
    return '%.1f MB' % (num_bytes / (1000 ** 2))
  elif num_bytes < 1000 ** 4:
    return '%.1f GB' % (num_bytes / (1000 ** 3))
  else:
    return '%.1f GB' % (num_bytes / (1000 ** 3))

