from __future__ import annotations
import progressbar
import numpy as np
import sys
import time
import util
from connectivity import ConnectivityDistribution
from typing import Any, Dict, List, Dict
from enum import Enum


class CureMethod(Enum):
  FIXED = 1
  GAUSSIAN = 2
  GEOMETRIC = 3


class State(Enum):
  UNINFECTED = 1
  INFECTED = 2
  IMMUNE = 3


class Parameters(object):
  size: int
  num_infected_start: int
  connectivity_distribution: ConnectivityDistribution
  connectivity: np.ndarray # Array of int connectivity values for each population member
  infection_rate: float
  infection_time: float
  infection_time_sigma: float
  cure_method: CureMethod
  dt: float
  end_time: float

  connectivity_cdf: np.ndarray # Cdf for probability of including each member in a group
  mean_n_value: float # Mean connectivity value
  mean_r_value: float # R value assuming entire population uninfected and uniform mean n value.
  eff_infection_rate: float

  def __init__(
    self,
    size: int,
    num_infected_start: int,
    connectivity_distribution: ConnectivityDistribution,
    dt: float,
    end_time: float,
    infection_rate: float,
    infection_time: float,
    infection_time_sigma: float,
    cure_method: CureMethod) -> None:
    self.connectivity_distribution = connectivity_distribution
    self.connectivity = connectivity_distribution.generate(size)
    self.size = size
    self.num_infected_start = num_infected_start
    self.dt = dt
    self.end_time = end_time
    self.infection_rate = infection_rate
    self.infection_time = infection_time
    self.infection_time_sigma = infection_time_sigma
    self.cure_method = cure_method

    self.connectivity_cdf = np.zeros(size)
    cum_prob = 0.0
    total_conn = np.sum(self.connectivity)
    for i, n in enumerate(self.connectivity):
      cum_prob += n / total_conn
      self.connectivity_cdf[i] = cum_prob

    self.eff_infection_rate = infection_rate * dt
    self.mean_n_value = np.average(self.connectivity)
    self.mean_r_value = self.mean_n_value * self.infection_rate * self.infection_time

  def __str__(self) -> str:
    return self.__repr__()

  def __repr__(self) -> str:
    return '\n'.join([
      '%20s: %d' % ('population size', self.size),
      '%20s: %.2f' % ('mean connectivity', self.mean_n_value),
      '%20s: %.2f' % ('mean r value', self.mean_r_value),
      '%20s: %.2f' % ('eff infection rate', self.eff_infection_rate),
      '%20s: %.2f' % ('dt', self.dt),
      '%20s: %.2f' % ('end time', self.end_time)
    ])

  def get_num_times(self) -> int:
    return int(np.ceil((self.end_time / self.dt + 1)))


class Output(object):
  num_times: int
  p: Parameters
  times: np.ndarray
  new_recoveries: np.ndarray # array of integers
  new_infections: np.ndarray # array of integers
  frac_uninfected: np.ndarray
  frac_infected: np.ndarray
  frac_immune: np.ndarray

  _write_index: int

  def __init__(
    self, p: Parameters, times: np.ndarray,
    frac_uninfected: np.ndarray, frac_infected: np.ndarray, frac_immune: np.ndarray,
    new_infections: np.ndarray, new_recoveries: np.ndarray) -> None:
    self.num_times = len(times)
    self.times = times
    self.p = p
    self.frac_uninfected = frac_uninfected
    self.frac_infected = frac_infected
    self.frac_immune = frac_immune
    self.new_infections = new_infections
    self.new_recoveries = new_recoveries
    self._write_index = 0

  @classmethod
  def estimate_size(cls, p: Parameters) -> int:
    """
    Returns estimated number of bytes for output.
    """
    float_size = 8
    return float_size * p.get_num_times() * 6

  @classmethod
  def make_new(cls, p: Parameters) -> Output:
    num_times = p.get_num_times()
    times = np.zeros(num_times)
    frac_infected = np.zeros(num_times)
    frac_uninfected = np.zeros(num_times)
    frac_immune = np.zeros(num_times)
    new_infections = np.zeros(num_times, dtype=int)
    new_recoveries = np.zeros(num_times, dtype=int)
    return Output(
      p=p, times=times,
      frac_uninfected=frac_uninfected, frac_infected=frac_infected, frac_immune=frac_immune,
      new_infections=new_infections, new_recoveries=new_recoveries)

  def write_state(self, index: int, time: float, states: np.ndarray, num_new_infections: int, num_new_recoveries: int) -> None:
    i = self._write_index
    self.times[i] = time
    self.frac_uninfected[i] = np.sum(states == State.UNINFECTED) / self.p.size
    self.frac_infected[i] = np.sum(states == State.INFECTED) / self.p.size
    self.frac_immune[i] = np.sum(states == State.IMMUNE) / self.p.size
    self.new_infections[i] = num_new_infections
    self.new_recoveries[i] = num_new_recoveries
    self._write_index += 1

  @classmethod
  def load(cls, fname) -> Output:
    d = dict(np.load(fname, allow_pickle=True))
    d['p'] = d['p'].item(0)
    return cls(**d)

  def save(self, fname) -> None:
    np.savez(fname,
      p=self.p,
      times=self.times,
      frac_uninfected=self.frac_uninfected,
      frac_infected=self.frac_infected,
      frac_immune=self.frac_immune,
      new_infections=self.new_infections,
      new_recoveries=self.new_recoveries)


class Simulation(object):
  p: Parameters
  time: float
  states: np.ndarray # Array of State values for each population member
  inf_start_time: np.array # Arr of float times indicating time infection began for each pop member
  inf_stop_time: np.array # Arr of float times indicating when an infected individual will be cured
  bar: progressbar.ProgressBar

  num_infected: int
  num_uninfected: int

  def __init__(self, p: Parameters) -> None:
    self.p = p
    self.bar = progressbar.ProgressBar()

  def supress_output(self) -> None:
    self.bar = progressbar.NullBar()

  def _reset(self) -> None:
    self.time = 0.0
    self.states = np.array([State.UNINFECTED for _ in range(self.p.size)])
    self.inf_start_time = np.zeros(self.p.size)
    self.inf_stop_time = np.array([self._compute_infection_stop_time() for _ in range(self.p.size)])
    infected = np.random.choice(list(range(self.p.size)), size=self.p.num_infected_start, replace=False)
    self.states[infected] = State.INFECTED
    self.num_infected = np.sum(self.states == State.INFECTED)
    self.num_uninfected = np.sum(self.states == State.UNINFECTED)

  def _compute_infection_stop_time(self) -> float:
    if self.p.cure_method == CureMethod.FIXED:
      return self.time + self.p.infection_time
    elif self.p.cure_method == CureMethod.GAUSSIAN:
      return self.time + np.random.normal(
        loc=self.p.infection_time,
        scale=self.p.infection_time_sigma)
    else:
      return 0.0

  def run(self, prompt: bool=True) -> Output:
    self._reset()
    num_new_infections = 0
    num_new_recoveries = 0
    output_size = Output.estimate_size(self.p)
    if prompt and output_size > (1000 ** 3):
      msg = 'Output size is ~%s, continue?' % util.human_size(output_size)
      if not util.prompt_yes_no(msg, default=False):
        sys.exit(0)
    output = Output.make_new(self.p)

    num_times = self.p.get_num_times()
    for index in self.bar(range(num_times)):
      output.write_state(
        index,
        self.time,
        self.states,
        num_new_infections=num_new_infections,
        num_new_recoveries=num_new_recoveries)
      if self.num_infected != 0:
        num_new_recoveries = self._handle_infected()
      else:
        num_new_recoveries = 0
      if self.num_uninfected != 0 and self.num_infected != 0:
        num_new_infections = self._handle_uninfected()
      else:
        num_new_infections = 0
      self.time = (index + 1) * self.p.dt

    return output

  def _handle_infected(self) -> int:
    num_new_recoveries = 0

    # For now we used a fixed amount of time after which all infectious people recover.
    for i, s in enumerate(self.states):
      if s != State.INFECTED:
        continue

      cured = False

      if self.p.cure_method == CureMethod.GEOMETRIC:
        if np.random.random() < 1 / self.p.infection_time:
          cured = True
      else:
        cured = self.time >= self.inf_stop_time[i]
      if cured:
        self.states[i] = State.IMMUNE
        self.num_infected -= 1
        num_new_recoveries += 1

    return num_new_recoveries

  def _form_group(self, size: int) -> np.ndarray:
    """
    Forms a group of given size from population with drawing weighted by connectivity CDF.
    """
    rands = np.random.random(size)
    values = np.zeros(size, dtype=int)
    for i, r in enumerate(rands):

      # Messy binary search
      low = 0
      high = len(self.p.connectivity_cdf)
      while True:
        search = int((high-low) / 2 + low)
        if r < self.p.connectivity_cdf[search]:
          high = search
        elif r > self.p.connectivity_cdf[search]:
          low = search
        else:
          break
        if low == high or low + 1 == high:
          search = low
          break
      values[i] = search
    return values

  def _handle_uninfected(self) -> int:
    """
    This version uses a fixed group size for each person determined by their connectivity and the
    groups are non-reflexive. i.e. person A can encounter person B without the reverse being true.
    This algorithm works as follows. For each person `i` with connectivity `n`, `n` random numbers
    are generated. Each of these `n` random numbers are matched to a person using a binary search of
    the population connectivity CDF.

    If `N` is the population size and `M` is the mean connectivity, then this algorithm runs in:
      O(N * M * Log(N))
    Algorithm has to run once for each `N` population member            -> N
    For each pop member, has to compute `M` group members               -> M
    For each group member has to perform a binary search of `N` values  -> Log(N)

    Nothing here can be easily vectorized.
    """

    num_new_infected = 0

    # At each time step, for each person `i`, pick a set `S_i(t)` of `n` people for person `i` to
    # interact with where `n` is the connectivity of person `i`. As a first pass, we don't require
    # interaction groups to be symmetric.
    for i, n in enumerate(self.p.connectivity):
      if self.states[i] != State.UNINFECTED:
        continue

      # choose `n` people for person `i` to possibly be infected by. TODO: prevent self-selection.
      group = self._form_group(n)
      group_states = self.states[group]
      infection_chance = np.sum(group_states == State.INFECTED) * self.p.eff_infection_rate

      if np.random.random() < infection_chance:
        self.states[i] = State.INFECTED
        self.inf_start_time[i] = self.time
        self.inf_stop_time[i] = self._compute_infection_stop_time()
        self.num_infected += 1
        self.num_uninfected -= 1
        num_new_infected += 1
    return num_new_infected
